// Generated code from Butter Knife. Do not modify!
package com.gnest.remember.view.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.gnest.remember.R;
import com.google.android.material.appbar.AppBarLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditMemoFragment_ViewBinding implements Unbinder {
  private EditMemoFragment target;

  @UiThread
  public EditMemoFragment_ViewBinding(EditMemoFragment target, View source) {
    this.target = target;

    target.memoEditTextView = Utils.findRequiredViewAsType(source, R.id.editTextMemo, "field 'memoEditTextView'", EditText.class);
    target.removeAlert = Utils.findRequiredViewAsType(source, R.id.bt_remove_alert, "field 'removeAlert'", ImageView.class);
    target.arrow = Utils.findRequiredViewAsType(source, R.id.date_picker_arrow, "field 'arrow'", ImageView.class);
    target.appBarLayout = Utils.findRequiredViewAsType(source, R.id.app_bar_layout, "field 'appBarLayout'", AppBarLayout.class);
    target.compactCalendarView = Utils.findRequiredViewAsType(source, R.id.compactcalendar_view, "field 'compactCalendarView'", CompactCalendarView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.datePickerButton = Utils.findRequiredViewAsType(source, R.id.date_picker_button, "field 'datePickerButton'", RelativeLayout.class);
    target.datePickerTextView = Utils.findRequiredViewAsType(source, R.id.date_picker_text_view, "field 'datePickerTextView'", TextView.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.primaryColor = ContextCompat.getColor(context, R.color.colorPrimary);
    target.alarmRemoveText = res.getString(R.string.alarm_remove_text);
    target.alarmSetText = res.getString(R.string.alarm_set_text);
  }

  @Override
  @CallSuper
  public void unbind() {
    EditMemoFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.memoEditTextView = null;
    target.removeAlert = null;
    target.arrow = null;
    target.appBarLayout = null;
    target.compactCalendarView = null;
    target.toolbar = null;
    target.datePickerButton = null;
    target.datePickerTextView = null;
  }
}
