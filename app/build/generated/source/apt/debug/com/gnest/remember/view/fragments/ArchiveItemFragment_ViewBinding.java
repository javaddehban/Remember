// Generated code from Butter Knife. Do not modify!
package com.gnest.remember.view.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import androidx.annotation.UiThread;
import com.gnest.remember.R;

public class ArchiveItemFragment_ViewBinding extends ListItemFragment_ViewBinding {
  @UiThread
  public ArchiveItemFragment_ViewBinding(ArchiveItemFragment target, View source) {
    super(target, source);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.noteUnarchiveMessage = res.getString(R.string.note_unarchived_message);
    target.noteUnarchiveMessage1 = res.getString(R.string.note_unarchived_message_1);
    target.noteUnarchiveMessage2 = res.getString(R.string.note_unarchived_message_2);
  }
}
