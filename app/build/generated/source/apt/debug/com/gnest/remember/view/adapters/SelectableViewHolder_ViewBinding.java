// Generated code from Butter Knife. Do not modify!
package com.gnest.remember.view.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gnest.remember.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectableViewHolder_ViewBinding implements Unbinder {
  private SelectableViewHolder target;

  @UiThread
  public SelectableViewHolder_ViewBinding(SelectableViewHolder target, View source) {
    this.target = target;

    target.textView = Utils.findRequiredViewAsType(source, R.id.memo_textView, "field 'textView'", TextView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.textScrollView, "field 'scrollView'", ScrollView.class);
    target.pin = Utils.findRequiredViewAsType(source, R.id.pin, "field 'pin'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectableViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.textView = null;
    target.scrollView = null;
    target.pin = null;
  }
}
