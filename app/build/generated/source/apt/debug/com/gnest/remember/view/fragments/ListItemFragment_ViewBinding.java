// Generated code from Butter Knife. Do not modify!
package com.gnest.remember.view.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gnest.remember.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListItemFragment_ViewBinding implements Unbinder {
  private ListItemFragment target;

  @UiThread
  public ListItemFragment_ViewBinding(ListItemFragment target, View source) {
    this.target = target;

    target.layout = Utils.findRequiredViewAsType(source, R.id.items_fragment, "field 'layout'", LinearLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.memo_list, "field 'recyclerView'", RecyclerView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.ItemFragmentToolbar, "field 'toolbar'", Toolbar.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.noteArchivedMessage = res.getString(R.string.note_archived_message);
    target.noteArchivedMessage1 = res.getString(R.string.note_archived_message_1);
    target.noteArchivedMessage2 = res.getString(R.string.note_archived_message_2);
    target.noteRemovedMessage = res.getString(R.string.note_removed_message);
    target.noteRemovedMessage1 = res.getString(R.string.note_removed_message_1);
    target.noteRemovedMessage2 = res.getString(R.string.note_removed_message_2);
    target.sendMemoIntentTitle = res.getString(R.string.send_memo_intent_title);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListItemFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.layout = null;
    target.recyclerView = null;
    target.toolbar = null;
  }
}
