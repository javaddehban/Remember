package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_gnest_remember_model_db_data_MemoRealmProxy extends com.gnest.remember.model.db.data.Memo
    implements RealmObjectProxy, com_gnest_remember_model_db_data_MemoRealmProxyInterface {

    static final class MemoColumnInfo extends ColumnInfo {
        long mIdIndex;
        long mMemoTextIndex;
        long mPositionIndex;
        long mColorIndex;
        long mAlarmDateIndex;
        long mIsAlarmSetIndex;
        long mSelectedIndex;
        long mExpandedIndex;

        MemoColumnInfo(OsSchemaInfo schemaInfo) {
            super(8);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Memo");
            this.mIdIndex = addColumnDetails("mId", "mId", objectSchemaInfo);
            this.mMemoTextIndex = addColumnDetails("mMemoText", "mMemoText", objectSchemaInfo);
            this.mPositionIndex = addColumnDetails("mPosition", "mPosition", objectSchemaInfo);
            this.mColorIndex = addColumnDetails("mColor", "mColor", objectSchemaInfo);
            this.mAlarmDateIndex = addColumnDetails("mAlarmDate", "mAlarmDate", objectSchemaInfo);
            this.mIsAlarmSetIndex = addColumnDetails("mIsAlarmSet", "mIsAlarmSet", objectSchemaInfo);
            this.mSelectedIndex = addColumnDetails("mSelected", "mSelected", objectSchemaInfo);
            this.mExpandedIndex = addColumnDetails("mExpanded", "mExpanded", objectSchemaInfo);
        }

        MemoColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new MemoColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final MemoColumnInfo src = (MemoColumnInfo) rawSrc;
            final MemoColumnInfo dst = (MemoColumnInfo) rawDst;
            dst.mIdIndex = src.mIdIndex;
            dst.mMemoTextIndex = src.mMemoTextIndex;
            dst.mPositionIndex = src.mPositionIndex;
            dst.mColorIndex = src.mColorIndex;
            dst.mAlarmDateIndex = src.mAlarmDateIndex;
            dst.mIsAlarmSetIndex = src.mIsAlarmSetIndex;
            dst.mSelectedIndex = src.mSelectedIndex;
            dst.mExpandedIndex = src.mExpandedIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private MemoColumnInfo columnInfo;
    private ProxyState<com.gnest.remember.model.db.data.Memo> proxyState;

    com_gnest_remember_model_db_data_MemoRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (MemoColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.gnest.remember.model.db.data.Memo>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$mId() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.mIdIndex);
    }

    @Override
    public void realmSet$mId(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'mId' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$mMemoText() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.mMemoTextIndex);
    }

    @Override
    public void realmSet$mMemoText(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.mMemoTextIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.mMemoTextIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.mMemoTextIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.mMemoTextIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$mPosition() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.mPositionIndex);
    }

    @Override
    public void realmSet$mPosition(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.mPositionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.mPositionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$mColor() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.mColorIndex);
    }

    @Override
    public void realmSet$mColor(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.mColorIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.mColorIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.mColorIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.mColorIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$mAlarmDate() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.mAlarmDateIndex);
    }

    @Override
    public void realmSet$mAlarmDate(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.mAlarmDateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.mAlarmDateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$mIsAlarmSet() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.mIsAlarmSetIndex);
    }

    @Override
    public void realmSet$mIsAlarmSet(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.mIsAlarmSetIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.mIsAlarmSetIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$mSelected() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.mSelectedIndex);
    }

    @Override
    public void realmSet$mSelected(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.mSelectedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.mSelectedIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$mExpanded() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.mExpandedIndex);
    }

    @Override
    public void realmSet$mExpanded(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.mExpandedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.mExpandedIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Memo", 8, 0);
        builder.addPersistedProperty("mId", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("mMemoText", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("mPosition", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("mColor", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("mAlarmDate", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("mIsAlarmSet", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("mSelected", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("mExpanded", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static MemoColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new MemoColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Memo";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Memo";
    }

    @SuppressWarnings("cast")
    public static com.gnest.remember.model.db.data.Memo createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.gnest.remember.model.db.data.Memo obj = null;
        if (update) {
            Table table = realm.getTable(com.gnest.remember.model.db.data.Memo.class);
            MemoColumnInfo columnInfo = (MemoColumnInfo) realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class);
            long pkColumnIndex = columnInfo.mIdIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("mId")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("mId"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_gnest_remember_model_db_data_MemoRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("mId")) {
                if (json.isNull("mId")) {
                    obj = (io.realm.com_gnest_remember_model_db_data_MemoRealmProxy) realm.createObjectInternal(com.gnest.remember.model.db.data.Memo.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_gnest_remember_model_db_data_MemoRealmProxy) realm.createObjectInternal(com.gnest.remember.model.db.data.Memo.class, json.getInt("mId"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'mId'.");
            }
        }

        final com_gnest_remember_model_db_data_MemoRealmProxyInterface objProxy = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) obj;
        if (json.has("mMemoText")) {
            if (json.isNull("mMemoText")) {
                objProxy.realmSet$mMemoText(null);
            } else {
                objProxy.realmSet$mMemoText((String) json.getString("mMemoText"));
            }
        }
        if (json.has("mPosition")) {
            if (json.isNull("mPosition")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mPosition' to null.");
            } else {
                objProxy.realmSet$mPosition((int) json.getInt("mPosition"));
            }
        }
        if (json.has("mColor")) {
            if (json.isNull("mColor")) {
                objProxy.realmSet$mColor(null);
            } else {
                objProxy.realmSet$mColor((String) json.getString("mColor"));
            }
        }
        if (json.has("mAlarmDate")) {
            if (json.isNull("mAlarmDate")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mAlarmDate' to null.");
            } else {
                objProxy.realmSet$mAlarmDate((long) json.getLong("mAlarmDate"));
            }
        }
        if (json.has("mIsAlarmSet")) {
            if (json.isNull("mIsAlarmSet")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mIsAlarmSet' to null.");
            } else {
                objProxy.realmSet$mIsAlarmSet((boolean) json.getBoolean("mIsAlarmSet"));
            }
        }
        if (json.has("mSelected")) {
            if (json.isNull("mSelected")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mSelected' to null.");
            } else {
                objProxy.realmSet$mSelected((boolean) json.getBoolean("mSelected"));
            }
        }
        if (json.has("mExpanded")) {
            if (json.isNull("mExpanded")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mExpanded' to null.");
            } else {
                objProxy.realmSet$mExpanded((boolean) json.getBoolean("mExpanded"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.gnest.remember.model.db.data.Memo createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.gnest.remember.model.db.data.Memo obj = new com.gnest.remember.model.db.data.Memo();
        final com_gnest_remember_model_db_data_MemoRealmProxyInterface objProxy = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("mId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mId((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mId' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("mMemoText")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mMemoText((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$mMemoText(null);
                }
            } else if (name.equals("mPosition")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mPosition((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mPosition' to null.");
                }
            } else if (name.equals("mColor")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mColor((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$mColor(null);
                }
            } else if (name.equals("mAlarmDate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mAlarmDate((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mAlarmDate' to null.");
                }
            } else if (name.equals("mIsAlarmSet")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mIsAlarmSet((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mIsAlarmSet' to null.");
                }
            } else if (name.equals("mSelected")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mSelected((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mSelected' to null.");
                }
            } else if (name.equals("mExpanded")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$mExpanded((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mExpanded' to null.");
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'mId'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.gnest.remember.model.db.data.Memo copyOrUpdate(Realm realm, com.gnest.remember.model.db.data.Memo object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.gnest.remember.model.db.data.Memo) cachedRealmObject;
        }

        com.gnest.remember.model.db.data.Memo realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.gnest.remember.model.db.data.Memo.class);
            MemoColumnInfo columnInfo = (MemoColumnInfo) realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class);
            long pkColumnIndex = columnInfo.mIdIndex;
            long rowIndex = table.findFirstLong(pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_gnest_remember_model_db_data_MemoRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.gnest.remember.model.db.data.Memo copy(Realm realm, com.gnest.remember.model.db.data.Memo newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.gnest.remember.model.db.data.Memo) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.gnest.remember.model.db.data.Memo realmObject = realm.createObjectInternal(com.gnest.remember.model.db.data.Memo.class, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) newObject).realmGet$mId(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        com_gnest_remember_model_db_data_MemoRealmProxyInterface realmObjectSource = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) newObject;
        com_gnest_remember_model_db_data_MemoRealmProxyInterface realmObjectCopy = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$mMemoText(realmObjectSource.realmGet$mMemoText());
        realmObjectCopy.realmSet$mPosition(realmObjectSource.realmGet$mPosition());
        realmObjectCopy.realmSet$mColor(realmObjectSource.realmGet$mColor());
        realmObjectCopy.realmSet$mAlarmDate(realmObjectSource.realmGet$mAlarmDate());
        realmObjectCopy.realmSet$mIsAlarmSet(realmObjectSource.realmGet$mIsAlarmSet());
        realmObjectCopy.realmSet$mSelected(realmObjectSource.realmGet$mSelected());
        realmObjectCopy.realmSet$mExpanded(realmObjectSource.realmGet$mExpanded());
        return realmObject;
    }

    public static long insert(Realm realm, com.gnest.remember.model.db.data.Memo object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.gnest.remember.model.db.data.Memo.class);
        long tableNativePtr = table.getNativePtr();
        MemoColumnInfo columnInfo = (MemoColumnInfo) realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class);
        long pkColumnIndex = columnInfo.mIdIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$mMemoText = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mMemoText();
        if (realmGet$mMemoText != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.mMemoTextIndex, rowIndex, realmGet$mMemoText, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.mPositionIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mPosition(), false);
        String realmGet$mColor = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mColor();
        if (realmGet$mColor != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.mColorIndex, rowIndex, realmGet$mColor, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.mAlarmDateIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mAlarmDate(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.mIsAlarmSetIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mIsAlarmSet(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.mSelectedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mSelected(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.mExpandedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mExpanded(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.gnest.remember.model.db.data.Memo.class);
        long tableNativePtr = table.getNativePtr();
        MemoColumnInfo columnInfo = (MemoColumnInfo) realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class);
        long pkColumnIndex = columnInfo.mIdIndex;
        com.gnest.remember.model.db.data.Memo object = null;
        while (objects.hasNext()) {
            object = (com.gnest.remember.model.db.data.Memo) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$mMemoText = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mMemoText();
            if (realmGet$mMemoText != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.mMemoTextIndex, rowIndex, realmGet$mMemoText, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.mPositionIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mPosition(), false);
            String realmGet$mColor = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mColor();
            if (realmGet$mColor != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.mColorIndex, rowIndex, realmGet$mColor, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.mAlarmDateIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mAlarmDate(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.mIsAlarmSetIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mIsAlarmSet(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.mSelectedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mSelected(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.mExpandedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mExpanded(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, com.gnest.remember.model.db.data.Memo object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.gnest.remember.model.db.data.Memo.class);
        long tableNativePtr = table.getNativePtr();
        MemoColumnInfo columnInfo = (MemoColumnInfo) realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class);
        long pkColumnIndex = columnInfo.mIdIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
        }
        cache.put(object, rowIndex);
        String realmGet$mMemoText = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mMemoText();
        if (realmGet$mMemoText != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.mMemoTextIndex, rowIndex, realmGet$mMemoText, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.mMemoTextIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.mPositionIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mPosition(), false);
        String realmGet$mColor = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mColor();
        if (realmGet$mColor != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.mColorIndex, rowIndex, realmGet$mColor, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.mColorIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.mAlarmDateIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mAlarmDate(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.mIsAlarmSetIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mIsAlarmSet(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.mSelectedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mSelected(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.mExpandedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mExpanded(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.gnest.remember.model.db.data.Memo.class);
        long tableNativePtr = table.getNativePtr();
        MemoColumnInfo columnInfo = (MemoColumnInfo) realm.getSchema().getColumnInfo(com.gnest.remember.model.db.data.Memo.class);
        long pkColumnIndex = columnInfo.mIdIndex;
        com.gnest.remember.model.db.data.Memo object = null;
        while (objects.hasNext()) {
            object = (com.gnest.remember.model.db.data.Memo) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mId());
            }
            cache.put(object, rowIndex);
            String realmGet$mMemoText = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mMemoText();
            if (realmGet$mMemoText != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.mMemoTextIndex, rowIndex, realmGet$mMemoText, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.mMemoTextIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.mPositionIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mPosition(), false);
            String realmGet$mColor = ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mColor();
            if (realmGet$mColor != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.mColorIndex, rowIndex, realmGet$mColor, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.mColorIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.mAlarmDateIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mAlarmDate(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.mIsAlarmSetIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mIsAlarmSet(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.mSelectedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mSelected(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.mExpandedIndex, rowIndex, ((com_gnest_remember_model_db_data_MemoRealmProxyInterface) object).realmGet$mExpanded(), false);
        }
    }

    public static com.gnest.remember.model.db.data.Memo createDetachedCopy(com.gnest.remember.model.db.data.Memo realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.gnest.remember.model.db.data.Memo unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.gnest.remember.model.db.data.Memo();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.gnest.remember.model.db.data.Memo) cachedObject.object;
            }
            unmanagedObject = (com.gnest.remember.model.db.data.Memo) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_gnest_remember_model_db_data_MemoRealmProxyInterface unmanagedCopy = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) unmanagedObject;
        com_gnest_remember_model_db_data_MemoRealmProxyInterface realmSource = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$mId(realmSource.realmGet$mId());
        unmanagedCopy.realmSet$mMemoText(realmSource.realmGet$mMemoText());
        unmanagedCopy.realmSet$mPosition(realmSource.realmGet$mPosition());
        unmanagedCopy.realmSet$mColor(realmSource.realmGet$mColor());
        unmanagedCopy.realmSet$mAlarmDate(realmSource.realmGet$mAlarmDate());
        unmanagedCopy.realmSet$mIsAlarmSet(realmSource.realmGet$mIsAlarmSet());
        unmanagedCopy.realmSet$mSelected(realmSource.realmGet$mSelected());
        unmanagedCopy.realmSet$mExpanded(realmSource.realmGet$mExpanded());

        return unmanagedObject;
    }

    static com.gnest.remember.model.db.data.Memo update(Realm realm, com.gnest.remember.model.db.data.Memo realmObject, com.gnest.remember.model.db.data.Memo newObject, Map<RealmModel, RealmObjectProxy> cache) {
        com_gnest_remember_model_db_data_MemoRealmProxyInterface realmObjectTarget = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) realmObject;
        com_gnest_remember_model_db_data_MemoRealmProxyInterface realmObjectSource = (com_gnest_remember_model_db_data_MemoRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$mMemoText(realmObjectSource.realmGet$mMemoText());
        realmObjectTarget.realmSet$mPosition(realmObjectSource.realmGet$mPosition());
        realmObjectTarget.realmSet$mColor(realmObjectSource.realmGet$mColor());
        realmObjectTarget.realmSet$mAlarmDate(realmObjectSource.realmGet$mAlarmDate());
        realmObjectTarget.realmSet$mIsAlarmSet(realmObjectSource.realmGet$mIsAlarmSet());
        realmObjectTarget.realmSet$mSelected(realmObjectSource.realmGet$mSelected());
        realmObjectTarget.realmSet$mExpanded(realmObjectSource.realmGet$mExpanded());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Memo = proxy[");
        stringBuilder.append("{mId:");
        stringBuilder.append(realmGet$mId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mMemoText:");
        stringBuilder.append(realmGet$mMemoText() != null ? realmGet$mMemoText() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mPosition:");
        stringBuilder.append(realmGet$mPosition());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mColor:");
        stringBuilder.append(realmGet$mColor() != null ? realmGet$mColor() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mAlarmDate:");
        stringBuilder.append(realmGet$mAlarmDate());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mIsAlarmSet:");
        stringBuilder.append(realmGet$mIsAlarmSet());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mSelected:");
        stringBuilder.append(realmGet$mSelected());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mExpanded:");
        stringBuilder.append(realmGet$mExpanded());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

}
