package io.realm;


public interface com_gnest_remember_model_db_data_MemoRealmProxyInterface {
    public int realmGet$mId();
    public void realmSet$mId(int value);
    public String realmGet$mMemoText();
    public void realmSet$mMemoText(String value);
    public int realmGet$mPosition();
    public void realmSet$mPosition(int value);
    public String realmGet$mColor();
    public void realmSet$mColor(String value);
    public long realmGet$mAlarmDate();
    public void realmSet$mAlarmDate(long value);
    public boolean realmGet$mIsAlarmSet();
    public void realmSet$mIsAlarmSet(boolean value);
    public boolean realmGet$mSelected();
    public void realmSet$mSelected(boolean value);
    public boolean realmGet$mExpanded();
    public void realmSet$mExpanded(boolean value);
}
