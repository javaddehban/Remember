# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:19
-keep class androidx.core.app.CoreComponentFactory { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:102
-keep class com.crashlytics.android.CrashlyticsInitProvider { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:19
-keep class com.gnest.remember.App { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:44
-keep class com.gnest.remember.services.AlarmReceiver { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:39
-keep class com.gnest.remember.services.AlarmService { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:29
-keep class com.gnest.remember.view.activity.MainActivity { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:62
-keep class com.squareup.leakcanary.DisplayLeakService { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:77
-keep class com.squareup.leakcanary.internal.DisplayLeakActivity { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:58
-keep class com.squareup.leakcanary.internal.HeapAnalyzerService { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:67
-keep class com.squareup.leakcanary.internal.LeakCanaryFileProvider { <init>(); }
# Referenced at /home/javad/Desktop/Remember-master/app/build/intermediates/merged_manifests/debug/x86_64/AndroidManifest.xml:91
-keep class com.squareup.leakcanary.internal.RequestStoragePermissionActivity { <init>(); }
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout-watch-v20/abc_alert_dialog_title_material.xml:53
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog_title.xml:41
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog_actions.xml:45
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog.xml:51
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_title_material.xml:57
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_material.xml:52
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_button_bar_material.xml:43
-keep class android.widget.Space { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_select_dialog_material.xml:23
-keep class androidx.appcompat.app.AlertController$RecycleListView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_action_menu_item_layout.xml:17
-keep class androidx.appcompat.view.menu.ActionMenuItemView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_expanded_menu_layout.xml:17
-keep class androidx.appcompat.view.menu.ExpandedMenuView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_popup_menu_item_layout.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_list_menu_item_layout.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_cascading_menu_item_layout.xml:20
-keep class androidx.appcompat.view.menu.ListMenuItemView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_toolbar.xml:27
-keep class androidx.appcompat.widget.ActionBarContainer { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_toolbar.xml:43
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_action_mode_bar.xml:19
-keep class androidx.appcompat.widget.ActionBarContextView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_toolbar.xml:17
-keep class androidx.appcompat.widget.ActionBarOverlayLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_action_menu_layout.xml:17
-keep class androidx.appcompat.widget.ActionMenuView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_activity_chooser_view.xml:19
-keep class androidx.appcompat.widget.ActivityChooserView$InnerLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_material.xml:18
-keep class androidx.appcompat.widget.AlertDialogLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog_actions.xml:26
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_button_bar_material.xml:26
-keep class androidx.appcompat.widget.ButtonBarLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_content_include.xml:19
-keep class androidx.appcompat.widget.ContentFrameLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog_title.xml:36
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_title_material.xml:45
-keep class androidx.appcompat.widget.DialogTitle { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_simple_overlay_action_mode.xml:23
-keep class androidx.appcompat.widget.FitWindowsFrameLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_simple.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_dialog_title_material.xml:22
-keep class androidx.appcompat.widget.FitWindowsLinearLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_search_view.xml:75
-keep class androidx.appcompat.widget.SearchView$SearchAutoComplete { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout/preference_widget_switch_compat.xml:20
-keep class androidx.appcompat.widget.SwitchCompat { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout-v26/abc_screen_toolbar.xml:37
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/fragment_settings.xml:8
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_item_list.xml:9
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_edit.xml:48
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_toolbar.xml:36
-keep class androidx.appcompat.widget.Toolbar { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_simple_overlay_action_mode.xml:32
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_screen_simple.xml:25
-keep class androidx.appcompat.widget.ViewStubCompat { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/nav_header.xml:2
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/fragment_item.xml:2
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/content_fragment_edit.xml:2
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_item_list.xml:2
-keep class androidx.constraintlayout.widget.ConstraintLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_bottom_sheet_dialog.xml:26
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_edit.xml:2
-keep class androidx.coordinatorlayout.widget.CoordinatorLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_alert_dialog.xml:40
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/appcompat-1.2.0-alpha01.aar/64bb85fefe9624ff8db08e3eef313c7c/res/layout/abc_alert_dialog_material.xml:41
-keep class androidx.core.widget.NestedScrollView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/activity_main.xml:2
-keep class androidx.drawerlayout.widget.DrawerLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout-v17/preference_widget_seekbar_material.xml:82
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout/preference_widget_seekbar.xml:66
-keep class androidx.preference.UnPressableLinearLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout-v21/preference_category_material.xml:33
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout-v17/preference_widget_seekbar_material.xml:41
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout-v17/preference_material.xml:41
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout-v17/preference_dropdown_material.xml:47
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout/preference_dropdown.xml:38
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout-v17/preference_category_material.xml:33
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout/preference.xml:32
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout/expand_button.xml:42
-keep class androidx.preference.internal.PreferenceImageView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/preference-1.0.0.aar/352a35ffd0bbc68cd07cb751359c48e3/res/layout/preference_recyclerview.xml:18
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_calendar_months.xml:18
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_calendar_horizontal.xml:29
# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/content_fragment_item_list.xml:10
-keep class androidx.recyclerview.widget.RecyclerView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_edit.xml:35
-keep class com.github.sundeepk.compactcalendarview.CompactCalendarView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_edit.xml:9
-keep class com.google.android.material.appbar.AppBarLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/app_bar_fragment_edit.xml:17
-keep class com.google.android.material.appbar.CollapsingToolbarLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_toolbar_surface.xml:16
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_toolbar_elevation.xml:16
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_toolbar_custom_background.xml:16
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_toolbar.xml:16
-keep class com.google.android.material.appbar.MaterialToolbar { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_picker_header_fullscreen.xml:36
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_calendar_month_navigation.xml:32
-keep class com.google.android.material.button.MaterialButton { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_reflow_chipgroup.xml:25
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_action_chip.xml:17
-keep class com.google.android.material.chip.Chip { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/test_reflow_chipgroup.xml:19
-keep class com.google.android.material.chip.ChipGroup { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_calendar_month.xml:18
-keep class com.google.android.material.datepicker.MaterialCalendarGridView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_bottom_navigation_item.xml:27
-keep class com.google.android.material.internal.BaselineLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_picker_header_toggle.xml:18
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_text_input_start_icon.xml:18
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_text_input_end_icon.xml:18
-keep class com.google.android.material.internal.CheckableImageButton { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_navigation_item.xml:17
-keep class com.google.android.material.internal.NavigationMenuItemView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_navigation_menu.xml:17
-keep class com.google.android.material.internal.NavigationMenuView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/Desktop/Remember-master/app/src/main/res/layout/activity_main.xml:19
-keep class com.google.android.material.navigation.NavigationView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_layout_snackbar.xml:18
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_layout_snackbar.xml:18
-keep class com.google.android.material.snackbar.Snackbar$SnackbarLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_layout_snackbar_include.xml:18
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/design_layout_snackbar_include.xml:18
-keep class com.google.android.material.snackbar.SnackbarContentLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_picker_text_input_date_range.xml:32
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_picker_text_input_date.xml:31
-keep class com.google.android.material.textfield.TextInputEditText { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_picker_text_input_date_range.xml:26
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/mtrl_picker_text_input_date.xml:26
-keep class com.google.android.material.textfield.TextInputLayout { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/text_view_without_line_height.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/text_view_with_theme_line_height.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/text_view_with_line_height_from_style.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/text_view_with_line_height_from_layout.xml:17
# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/material-1.2.0-alpha03.aar/bee9a86e1e43f8647db5582125870d61/res/layout/text_view_with_line_height_from_appearance.xml:17
-keep class com.google.android.material.textview.MaterialTextView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/jetified-leakcanary-android-1.6.1.aar/5e7eb56e876eae0047085716ae1a42b1/res/layout/leak_canary_ref_row.xml:26
-keep class com.squareup.leakcanary.internal.DisplayLeakConnectorView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/jetified-leakcanary-android-1.6.1.aar/5e7eb56e876eae0047085716ae1a42b1/res/layout/leak_canary_ref_row.xml:32
-keep class com.squareup.leakcanary.internal.MoreDetailsView { <init>(android.content.Context, android.util.AttributeSet); }

# Referenced at /home/javad/.gradle/caches/transforms-1/files-1.1/jetified-leakcanary-android-1.6.1.aar/5e7eb56e876eae0047085716ae1a42b1/res/layout/leak_canary_ref_row.xml:17
-keep class com.squareup.leakcanary.internal.RowElementLayout { <init>(android.content.Context, android.util.AttributeSet); }

